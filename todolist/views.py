from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.template import loader

# We import the built-in 'User' model to be able to do operations to the built-in Users table from Django.
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone

from .models import ToDoItem, Event
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, AddEventForm, UpdateProfileForm

# Create your views here.
def index(request):
	todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
	event_list = Event.objects.filter(user_id=request.user.id)

	template = loader.get_template('todolist/index.html')
	context = {
		'todoitem_list': todoitem_list,
		'event_list': event_list, 
		'user': request.user
	}

	return HttpResponse(template.render(context, request))

	# You can also render a template using this way:
	# return render(request, 'todolist/index.html', context)
	

def todoitem(request, todoitem_id):
	# 'model_to_dict' function translates a data model into a python dictionary (object). This is so that we can use the data as a regular onject/dictionary.
	todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)

	return render(request, "todolist/todoitem.html", model_to_dict(todoitem))

# Register
def register(request):
	users = User.objects.all()
	is_user_registered = False
	
	# Loops through each existing user and checks if they already exists in the table.
	for individual_user in users:
		if individual_user.username == "ninz":
			is_user_registered = True
			break

	# The context 'is_user-registered' is put after the for loop in order for it to get the updated value if 'for' loop above equals to True.
	context = {
		"is_user_registered": is_user_registered
	}

	if is_user_registered == False:
		# 1. Creating of new instance of the User model.
		user = User()

		# 2. Assign data values to each property of the model.
		user.username = "ninz"
		user.first_name = "Ninz"
		user.last_name = "Naag"
		user.email = "ninz@mail.com"
		user.set_password("ninz1234")
		user.is_staff = False
		user.is_active = True

		# 3. Save the new instance of a user into the database.
		user.save()

		# The context will be the data to be passed to the template.
		context = {
		"first_name": user.first_name,
		"last_name": user.last_name
		}

	return render(request, "todolist/register.html", context)

# Update Profile (CAPSTONE)
def update_profile(request):

	form = UpdateProfileForm(request.POST)

	context = {
		"is_user_authenticated": False,
		"first_name": "",
		"last_name": "",
		"message": ""
	}

	if request.session.get("user_id") is not None:
		authenticated_user = User.objects.get(pk=request.session.get("user_id"))
	
		context["first_name"] = authenticated_user.first_name
		context["last_name"] = authenticated_user.last_name

		if request.method == 'POST':
			
			if form.is_valid() == False:
				form = UpdateProfileForm()

			else:

				username = authenticated_user.username
				password = form.cleaned_data["current_password"]
				new_password = form.cleaned_data["new_password"]
				confirm_password = form.cleaned_data["confirm_password"]
		
				if authenticated_user is not None and new_password == confirm_password:
					
					authenticated_user.first_name = form.cleaned_data["first_name"]
					authenticated_user.last_name = form.cleaned_data["last_name"]

					if new_password != "":			
						authenticated_user.set_password(form.cleaned_data["new_password"])

					authenticated_user.save()

					context['message'] = "User profile successfully updated."

					return render(request, "todolist/update_profile.html", context)

				else:
					context = {
						"error": True
					}

	return render(request, "todolist/update_profile.html", context)


# Login
def login_view(request):
	context = {
		"username": "",
		"password": ""
	}

	if request.method == 'POST':
		# request.POST contains all the fields coming from the Login.html form
		form  = LoginForm(request.POST)

		if form.is_valid() == False:
			# if you use LoginForm() without passing any data, it will just return the properties 'username' and 'password' without any data/value
			form = LoginForm()

		else:
			# we use the 'form.cleaned_data[]' to get the properties that we need from the form
			# we can use the properties declared in the form.py within the LoginForm class to get their values.
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
		
			user = authenticate(username=username, password=password)

			context = {
				"username": username,
				"password": password
			}

			if user is not None:
				# The 'login' function saves the user's ID in the Django's session. The 'request' will be the one that handles the user's data and we can access the user by using 'request.user'
				login(request, user)

				request.session['user_id'] = user.id
				request.session['username'] = username

				return redirect("todolist:index")
			else:
				context = {
					"error": True
				}

	return render(request, "todolist/login.html", context)

# Logout
def logout_view(request):
	logout(request)
	return redirect("todolist:index")

# Add task
def add_task(request):
	context = {}

	if request.method == 'POST':
		form = AddTaskForm(request.POST)

		if form.is_valid() == False:
			form = AddTaskForm()

		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			duplicates = ToDoItem.objects.filter(task_name=task_name)

			if not duplicates:
				ToDoItem.objects.create(
					task_name=task_name,
					description=description,
					date_created=timezone.now(),
					user_id=request.user.id # ID of the logged in user
				)

				return redirect('todolist:index')

			else:
				context = {
				"error": True
			}
	return render(request, 'todolist/add_task.html', context)

# Update a task
def update_task(request, todoitem_id):
	 # Returns a queryset
    todoitem = ToDoItem.objects.filter(pk=todoitem_id)

    context = {
        "user": request.user,
        "todoitem_id": todoitem_id,
        # Accessing the first element is necessary because the "ToDoItem.objects.filter()" method returns a queryset
        "task_name": todoitem[0].task_name,
        "description": todoitem[0].description,
        "status": todoitem[0].status
    }

    if request.method == 'POST':

        form = UpdateTaskForm(request.POST)

        if form.is_valid() == False:

            form = UpdateTaskForm()

        else:

            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']
            
            if todoitem:

                todoitem[0].task_name = task_name
                todoitem[0].description = description
                todoitem[0].status = status

                todoitem[0].save()
                return redirect("todolist:index")

            else:

                context = {
                    "error": True
                }

    return render(request, "todolist/update_task.html", context)

# Delete a task
def delete_task(request, todoitem_id):
	todoitem = ToDoItem.objects.filter(pk=todoitem_id)

	todoitem.delete()

	return redirect("todolist:index")

# Add an event (CAPSTONE)
def event(request, event_id):
	# 'model_to_dict' function translates a data model into a python dictionary (object). This is so that we can use the data as a regular onject/dictionary.
	event = get_object_or_404(Event, pk=event_id)

	return render(request, "todolist/event.html", model_to_dict(event))

# Add an event
def add_event(request):
	context = {}

	if request.method == 'POST':
		form = AddEventForm(request.POST)

		if form.is_valid() == False:
			form = AddEventForm()

		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']

			duplicates = Event.objects.filter(event_name=event_name)

			if not duplicates:
				Event.objects.create(
					event_name=event_name,
					description=description,
					event_date=timezone.now(),
					user_id=request.user.id # ID of the logged in user
				)

				return redirect('todolist:index')

			else:
				context = {
				"error": True
			}
	return render(request, 'todolist/add_event.html', context)	

